package com.rydedispatch.driver.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.rydedispatch.driver.R;
import com.rydedispatch.driver.adpater.HistoryAdapter;
import com.rydedispatch.driver.asynctask.SimpleRequest;
import com.rydedispatch.driver.model.History;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.rydedispatch.driver.utility.AppConfig.URL_HISTORY_LIST;

public class HistoryFragment extends Fragment {

    private ArrayList<History> histories = new ArrayList<>();
    private RecyclerView rvHistory;
    private TextView tv_list;
    private int currentPage = 1;
    private int lastPage = 1;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_history, container, false);

        tv_list = rootView.findViewById(R.id.tv_list);
        rvHistory = rootView.findViewById(R.id.rv_history);
        rvHistory.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvHistory.setItemAnimator(new DefaultItemAnimator());
        rvHistory.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (currentPage == lastPage) {
                    return;
                }
                getHistory(++currentPage);
            }
        });


        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        getHistory(currentPage);
    }

    private void getHistory(int page){
        if(page == 1){
            histories =  new ArrayList<>();
        }

        SimpleRequest paramsRequest = new SimpleRequest(getActivity(), URL_HISTORY_LIST, new SimpleRequest.Callbacks() {
            @Override
            public void onFail(String error) {
                Toast.makeText(getActivity(),error, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onSuccess(JSONObject response) {
                try {
                    JSONObject data = response.getJSONObject("data");
                    JSONObject history = data.getJSONObject("history");
                    lastPage = history.getInt("last_page");
                    JSONArray data1 = history.getJSONArray("data");
                    for (int i=0; i<data1.length(); i++){
                        JSONObject jsonObject = data1.getJSONObject(i);
                        String firstName = jsonObject.getString("first_name");
                        String last_name = jsonObject.getString("last_name");
                        String profile_picture = jsonObject.getString("profile_picture");
                        String user_unique_code = jsonObject.getString("user_unique_code");
                        int id = jsonObject.getInt("id");
                        int sub_user_id = jsonObject.getInt("sub_user_id");
                        String display_passenger_name = jsonObject.getString("display_passenger_name");
                        String passenger_phone_number = jsonObject.getString("passenger_phone_number");
                        String source_location = jsonObject.getString("source_location");
                        String source_latitude = jsonObject.getString("source_latitude");
                        String source_longitude = jsonObject.getString("source_longitude");
                        String destination_location = jsonObject.getString("destination_location");
                        String destination_latitude = jsonObject.getString("destination_latitude");
                        String destination_longitude = jsonObject.getString("destination_longitude");
                        String payment_type = jsonObject.getString("payment_type");
                        String status = jsonObject.getString("status");
                        String completed_time = jsonObject.getString("completed_time");
                        String total_cost = jsonObject.getString("total_cost");
                        String actual_cost_without_commission = jsonObject.getString("actual_cost_without_commission");
                        String commission = jsonObject.getString("commission");
                        String schedule_date = jsonObject.getString("schedule_date");
                        int is_schedule = jsonObject.getInt("is_schedule");
                        String ride_instruction = jsonObject.getString("ride_instruction");

                        histories.add(new History(firstName,last_name,profile_picture,user_unique_code,id,sub_user_id,display_passenger_name,passenger_phone_number,source_location,source_latitude,source_longitude,
                                destination_location,destination_latitude,destination_longitude,payment_type,status,completed_time,total_cost,actual_cost_without_commission,commission,schedule_date,is_schedule,ride_instruction));
                    }

                    if(histories.size()>0){
                        HistoryAdapter historyAdapter = new HistoryAdapter(getActivity(),histories);
                        rvHistory.setAdapter(historyAdapter);
                        tv_list.setVisibility(View.GONE);
                    }else{
                        rvHistory.setVisibility(View.GONE);
                        tv_list.setVisibility(View.VISIBLE);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        paramsRequest.doInBackEnd();
    }
}
