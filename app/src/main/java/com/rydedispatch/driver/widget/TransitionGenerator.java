package com.rydedispatch.driver.widget;

import android.graphics.RectF;

interface TransitionGenerator {
    Transition generateNextTransition(RectF drawableBounds, RectF viewport);
}
