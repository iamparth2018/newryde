package com.rydedispatch.driver.model;

public class History {

    private String firstName;
    private String lastName;
    private String profilePicture;
    private String user_unique_code;
    private int id;
    private int sub_user_id;
    private String display_passenger_name;
    private String passenger_phone_number;
    private String sourceLocation;
    private String source_latitude;
    private String source_longitude;
    private String destinationLocation;
    private String destination_latitude;
    private String destination_longitude;
    private String paymentType;
    private String status;
    private String completedTime;
    private String totalCost;
    private String actual_cost_without_commission;
    private String commission;
    private String scheduleDate;
    private int isSchedule;
    private String rideInstruction;

    public History(String firstName, String lastName, String profilePicture, String user_unique_code, int id, int sub_user_id, String display_passenger_name, String passenger_phone_number, String sourceLocation, String source_latitude, String source_longitude, String destinationLocation, String destination_latitude, String destination_longitude, String paymentType, String status, String completedTime, String totalCost, String actual_cost_without_commission, String commission, String scheduleDate, int isSchedule, String rideInstruction) {

        this.firstName = firstName;
        this.lastName = lastName;
        this.profilePicture = profilePicture;
        this.user_unique_code = user_unique_code;
        this.id = id;
        this.sub_user_id = sub_user_id;
        this.display_passenger_name = display_passenger_name;
        this.passenger_phone_number = passenger_phone_number;
        this.sourceLocation = sourceLocation;
        this.source_latitude = source_latitude;
        this.source_longitude = source_longitude;
        this.destinationLocation = destinationLocation;
        this.destination_latitude = destination_latitude;
        this.destination_longitude = destination_longitude;
        this.paymentType = paymentType;
        this.status = status;
        this.completedTime = completedTime;
        this.totalCost = totalCost;
        this.actual_cost_without_commission = actual_cost_without_commission;
        this.commission = commission;
        this.scheduleDate = scheduleDate;
        this.isSchedule = isSchedule;
        this.rideInstruction = rideInstruction;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public String getUser_unique_code() {
        return user_unique_code;
    }

    public void setUser_unique_code(String user_unique_code) {
        this.user_unique_code = user_unique_code;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSub_user_id() {
        return sub_user_id;
    }

    public void setSub_user_id(int sub_user_id) {
        this.sub_user_id = sub_user_id;
    }

    public String getDisplay_passenger_name() {
        return display_passenger_name;
    }

    public void setDisplay_passenger_name(String display_passenger_name) {
        this.display_passenger_name = display_passenger_name;
    }

    public String getPassenger_phone_number() {
        return passenger_phone_number;
    }

    public void setPassenger_phone_number(String passenger_phone_number) {
        this.passenger_phone_number = passenger_phone_number;
    }

    public String getSourceLocation() {
        return sourceLocation;
    }

    public void setSourceLocation(String sourceLocation) {
        this.sourceLocation = sourceLocation;
    }

    public String getSource_latitude() {
        return source_latitude;
    }

    public void setSource_latitude(String source_latitude) {
        this.source_latitude = source_latitude;
    }

    public String getSource_longitude() {
        return source_longitude;
    }

    public void setSource_longitude(String source_longitude) {
        this.source_longitude = source_longitude;
    }

    public String getDestinationLocation() {
        return destinationLocation;
    }

    public void setDestinationLocation(String destinationLocation) {
        this.destinationLocation = destinationLocation;
    }

    public String getDestination_latitude() {
        return destination_latitude;
    }

    public void setDestination_latitude(String destination_latitude) {
        this.destination_latitude = destination_latitude;
    }

    public String getDestination_longitude() {
        return destination_longitude;
    }

    public void setDestination_longitude(String destination_longitude) {
        this.destination_longitude = destination_longitude;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCompletedTime() {
        return completedTime;
    }

    public void setCompletedTime(String completedTime) {
        this.completedTime = completedTime;
    }

    public String getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(String totalCost) {
        this.totalCost = totalCost;
    }

    public String getActual_cost_without_commission() {
        return actual_cost_without_commission;
    }

    public void setActual_cost_without_commission(String actual_cost_without_commission) {
        this.actual_cost_without_commission = actual_cost_without_commission;
    }

    public String getCommission() {
        return commission;
    }

    public void setCommission(String commission) {
        this.commission = commission;
    }

    public String getScheduleDate() {
        return scheduleDate;
    }

    public void setScheduleDate(String scheduleDate) {
        this.scheduleDate = scheduleDate;
    }

    public int getIsSchedule() {
        return isSchedule;
    }

    public void setIsSchedule(int isSchedule) {
        this.isSchedule = isSchedule;
    }

    public String getRideInstruction() {
        return rideInstruction;
    }

    public void setRideInstruction(String rideInstruction) {
        this.rideInstruction = rideInstruction;
    }
}
