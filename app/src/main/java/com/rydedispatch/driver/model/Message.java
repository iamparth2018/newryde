package com.rydedispatch.driver.model;

public class Message {

    private String message;
    private int direction;
    private String is_read;
    private String updated_at;
    private String first_name;
    private String last_name;
    private String profile_picture;

    public Message(String message, int direction, String is_read, String updated_at, String first_name, String last_name, String profile_picture) {
        this.message = message;
        this.direction = direction;
        this.is_read = is_read;
        this.updated_at = updated_at;
        this.first_name = first_name;
        this.last_name = last_name;
        this.profile_picture = profile_picture;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getDirection() {
        return direction;
    }

    public void setDirection(int direction) {
        this.direction = direction;
    }

    public String getIs_read() {
        return is_read;
    }

    public void setIs_read(String is_read) {
        this.is_read = is_read;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getProfile_picture() {
        return profile_picture;
    }

    public void setProfile_picture(String profile_picture) {
        this.profile_picture = profile_picture;
    }
}
