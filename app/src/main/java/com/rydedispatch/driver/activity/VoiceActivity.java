package com.rydedispatch.driver.activity;

import android.Manifest;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.media.AudioAttributes;
import android.media.AudioFocusRequest;
import android.media.AudioManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.os.Vibrator;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Chronometer;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.firebase.iid.FirebaseInstanceId;
import com.rydedispatch.driver.R;
import com.rydedispatch.driver.asynctask.SimpleRequest;
import com.rydedispatch.driver.utility.PreferenceHelper;
import com.twilio.voice.Call;
import com.twilio.voice.CallException;
import com.twilio.voice.CallInvite;
import com.twilio.voice.ConnectOptions;
import com.twilio.voice.RegistrationException;
import com.twilio.voice.RegistrationListener;
import com.twilio.voice.Voice;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Locale;

import static com.rydedispatch.driver.utility.AppConfig.URL_GET_AUDIO_TOKEN;


public class VoiceActivity extends AppCompatActivity {

    private static final String TAG = "VoiceActivity";

    private static final int MIC_PERMISSION_REQUEST_CODE = 1;

    private String accessToken = "";
    private AudioManager audioManager;
    private int savedAudioMode = AudioManager.MODE_INVALID;
    private LinearLayout incoming;
    private RelativeLayout call;
    private NotificationManager notificationManager;
    private boolean isReceiverRegistered = false;
    private VoiceBroadcastReceiver voiceBroadcastReceiver;
    private int activeCallNotificationId;
    private boolean isConnect = false;

    // Empty HashMap, never populated for the Quickstart
    private HashMap<String, String> params = new HashMap<>();

    private ToggleButton muteActionFab;
    private ToggleButton speakerActionFab;
    private Chronometer chronometer;
    private TextView tatClientName;
    private TextView txtReaderName;

    private Ringtone mRingtone;
    private Vibrator vibrator;

    public static final String INCOMING_CALL_INVITE = "INCOMING_CALL_INVITE";
    public static final String CANCELLED_CALL_INVITE = "CANCELLED_CALL_INVITE";
    public static final String INCOMING_CALL_NOTIFICATION_ID = "INCOMING_CALL_NOTIFICATION_ID";
    public static final String ACTION_INCOMING_CALL = "ACTION_INCOMING_CALL";
    public static final String ACTION_CANCEL_CALL = "ACTION_CANCEL_CALL";
    private static final String ACTION_FCM_TOKEN = "ACTION_FCM_TOKEN";

    private CallInvite activeCallInvite;
    private Call activeCall;

    private RegistrationListener registrationListener = registrationListener();
    private Call.Listener callListener = callListener();

    private PreferenceHelper helper;
    private int checkoutgoing = 0;
    private String payment_type;
    private String passenger_phone_number;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_outgoing);

        helper = new PreferenceHelper(this);
        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
                | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        Intent i = getIntent();
        checkoutgoing = i.getIntExtra("checkOutgoing", 0);

        if(i.hasExtra("payment_type")){
            payment_type = i.getStringExtra("payment_type");
            passenger_phone_number = i.getStringExtra("passenger_phone_number");
        }

        accessToken = helper.getTwilioAuth();

        incoming = findViewById(R.id.incoming);
        call = findViewById(R.id.call);

        ImageButton hangupActionFab = findViewById(R.id.hangup_action_fab);
        ImageButton connect_call = findViewById(R.id.connect_call);
        ImageButton connect_disconnect = findViewById(R.id.connect_disconnect);
        muteActionFab = findViewById(R.id.tb_mute);
        speakerActionFab = findViewById(R.id.tb_speaker);
        chronometer = findViewById(R.id.chronometer);
        tatClientName = findViewById(R.id.txt_clinet_name);
        txtReaderName = findViewById(R.id.txt_reader_name);

        hangupActionFab.setOnClickListener(hangupActionFabClickListener());
        connect_call.setOnClickListener(answerCallClickListener());
        connect_disconnect.setOnClickListener(cancelCallClickListener());

        muteActionFab.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (activeCall != null) {
                    boolean mute = !activeCall.isMuted();
                    activeCall.mute(mute);
                    muteActionFab.setChecked(mute);
                }
            }
        });

        voiceBroadcastReceiver = new VoiceBroadcastReceiver();
        registerReceiver();

        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        audioManager.setSpeakerphoneOn(true);

        speakerActionFab.setChecked(audioManager.isSpeakerphoneOn());
        speakerActionFab.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                speakerActionFab.setChecked(isChecked);
                audioManager.setSpeakerphoneOn(isChecked);
            }
        });


        setVolumeControlStream(AudioManager.STREAM_VOICE_CALL);

        handleIncomingCallIntent(getIntent());

        if (!checkPermissionForMicrophone()) {
            requestPermissionForMicrophone();
        } else {
            registerForCallInvites();
        }

        txtReaderName.setText(R.string.connecting);

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        handleIncomingCallIntent(intent);
    }

    private RegistrationListener registrationListener() {
        return new RegistrationListener() {
            @Override
            public void onRegistered(@NonNull String accessToken, @NonNull String fcmToken) {
                Log.d(TAG, "Successfully registered FCM " + fcmToken + " " + checkoutgoing);
                if (checkoutgoing == 1 || checkoutgoing == 3) {
                    makeCall();
                }
            }

            @Override
            public void onError(@NonNull RegistrationException error, @NonNull String accessToken, @NonNull String fcmToken) {
                String message = String.format(Locale.US,"Registration Error: %d, %s", error.getErrorCode(), error.getMessage());
                Log.e(TAG, message);
            }
        };
    }

    private Call.Listener callListener() {
        return new Call.Listener() {
            @Override
            public void onRinging(@NonNull Call call) {
                txtReaderName.setText(R.string.ringing);
                Log.d(TAG, "Ringing");
            }

            @Override
            public void onConnectFailure(@NonNull Call call, @NonNull CallException error) {
                setAudioFocus(false);
                Log.d(TAG, "Connect failure");
                String message = String.format(Locale.US,"Call Error: %d, %s", error.getErrorCode(), error.getMessage());
                Log.e(TAG, message);
                resetUI();
            }

            @Override
            public void onConnected(@NonNull Call call) {
                setAudioFocus(true);
                Log.d(TAG, "Connected");
                activeCall = call;
                if (checkoutgoing == 1) {
                    txtReaderName.setText(R.string.call_no);
                } else if (checkoutgoing == 3) {
                    txtReaderName.setText(helper.getDriverName());
                }else{
                    txtReaderName.setText(activeCall.getFrom().replace("client:", ""));
                }

                chronometer.setVisibility(View.VISIBLE);
                chronometer.setBase(SystemClock.elapsedRealtime());
                chronometer.start();
            }

            @Override
            public void onReconnecting(@NonNull Call call, @NonNull CallException callException) {
                Log.d(TAG, "onReconnecting");
                txtReaderName.setText(R.string.reconnecting);
            }

            @Override
            public void onReconnected(@NonNull Call call) {
                Log.d(TAG, "onReconnected");
                txtReaderName.setText(R.string.reconnected);
            }

            @Override
            public void onDisconnected(@NonNull Call call, CallException error) {
                setAudioFocus(false);
                Log.d(TAG, "Disconnected");
                if (error != null) {
                    String message = String.format(Locale.US,"Call Error: %d, %s", error.getErrorCode(), error.getMessage());
                    Log.e(TAG, message);
                }
                resetUI();
            }
        };
    }

    private void setCallUI() {
        incoming.setVisibility(View.GONE);
        call.setVisibility(View.VISIBLE);
        chronometer.setVisibility(View.VISIBLE);
        chronometer.setBase(SystemClock.elapsedRealtime());
        chronometer.start();
    }

    /*
     * Reset UI elements
     */
    private void resetUI() {
        dismissVibrate();
        finish();
        incoming.setVisibility(View.GONE);
        call.setVisibility(View.VISIBLE);
        chronometer.setVisibility(View.INVISIBLE);
        chronometer.stop();

    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver();
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver();
    }

    private void handleIncomingCallIntent(Intent intent) {
        if (intent != null && intent.getAction() != null) {
            Log.e(TAG,intent.getAction());
            switch (intent.getAction()) {
                case ACTION_INCOMING_CALL:
                    activeCallInvite = intent.getParcelableExtra(INCOMING_CALL_INVITE);
                    if (activeCallInvite != null) {
                        incoming.setVisibility(View.VISIBLE);
                        call.setVisibility(View.GONE);
                        activeCallNotificationId = intent.getIntExtra(INCOMING_CALL_NOTIFICATION_ID, 0);
                        tatClientName.setText(String.format("%s is calling", activeCallInvite.getFrom().replace("client:", "")));
                    }
                    startRingtone();
                    break;
                case ACTION_CANCEL_CALL:
                    if (!isConnect) {
                        incoming.setVisibility(View.GONE);
                        call.setVisibility(View.VISIBLE);
                        resetUI();
                    }
                    dismissVibrate();
                    break;
                case ACTION_FCM_TOKEN:
                    registerForCallInvites();
                    break;
            }
        }
    }

    private void startRingtone() {
        Uri defaultRintoneUri = RingtoneManager.getActualDefaultRingtoneUri(this
                , RingtoneManager.TYPE_RINGTONE);
        vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        mRingtone = RingtoneManager.getRingtone(this, defaultRintoneUri);
        long[] pattern = {0, 100, 200, 300, 400};
        if (mRingtone != null)
            mRingtone.play();
        vibrator.vibrate(pattern, 0);
    }

    private void dismissVibrate() {
        if (vibrator != null) {
            vibrator.cancel();
            vibrator = null;
        }

        if (mRingtone != null) {
            mRingtone.stop();
            mRingtone = null;
        }

    }

    private void registerReceiver() {
        if (!isReceiverRegistered) {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(ACTION_INCOMING_CALL);
            intentFilter.addAction(ACTION_CANCEL_CALL);
            intentFilter.addAction(ACTION_FCM_TOKEN);
            LocalBroadcastManager.getInstance(this).registerReceiver(
                    voiceBroadcastReceiver, intentFilter);
            isReceiverRegistered = true;
        }
    }

    private void unregisterReceiver() {
        if (isReceiverRegistered) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(voiceBroadcastReceiver);
            isReceiverRegistered = false;
        }
    }

    private class VoiceBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(ACTION_INCOMING_CALL) || action.equals(ACTION_CANCEL_CALL)) {
                handleIncomingCallIntent(intent);
            }
        }
    }


    private void registerForCallInvites() {
        final String fcmToken = FirebaseInstanceId.getInstance().getToken();
        if (fcmToken != null && accessToken != null && accessToken.length()>0) {
            Log.i(TAG, "Registering with FCM");
            Voice.register(accessToken, Voice.RegistrationChannel.FCM, fcmToken, registrationListener);
        }else{
            getToken();
        }
    }

    private void getToken(){
        SimpleRequest simpleRequest = new SimpleRequest(this, URL_GET_AUDIO_TOKEN, new SimpleRequest.Callbacks() {

            @Override
            public void onFail(String error) {
                Toast.makeText(getApplicationContext(),error, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onSuccess(JSONObject response) {
                try {
                    JSONObject data = response.getJSONObject("data");
                    accessToken = data.getString("token");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            registerForCallInvites();
                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        simpleRequest.doInBackEnd();
    }

   private void makeCall(){
       if (checkoutgoing == 1) {
           params.put("PhoneNumber", "+12015550000");
           params.put("call_type", "support");
//           params.put("to", "+12015550000");
           params.put("fromnumber", helper.getUserUniqueCode());
           txtReaderName.setText(R.string.call_no);
       } else if (checkoutgoing == 3) {
           if(payment_type.equalsIgnoreCase("none")){
               params.put("PhoneNumber", helper.getDriverUniqueCode());
               params.put("call_type", "normal");
//           params.put("to", helper.getDriverUniqueCode());
               params.put("fromnumber", passenger_phone_number);
           }else{
               params.put("PhoneNumber", helper.getDriverUniqueCode());
               params.put("call_type", "normal");
//           params.put("to", helper.getDriverUniqueCode());
               params.put("fromnumber",  helper.getUserUniqueCode());
           }
           txtReaderName.setText(helper.getDriverName());
       }

       ConnectOptions connectOptions = new ConnectOptions.Builder(accessToken)
               .params(params)
               .build();
       activeCall = Voice.connect(VoiceActivity.this, connectOptions, callListener);
       setCallUI();
   }

    private View.OnClickListener hangupActionFabClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetUI();
                disconnect();
            }
        };
    }

    private View.OnClickListener answerCallClickListener() {
        return new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                isConnect = true;
                answer();
                dismissVibrate();
                setCallUI();
            }
        };
    }

    private View.OnClickListener cancelCallClickListener() {
        return new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (activeCallInvite != null) {
                    activeCallInvite.reject(VoiceActivity.this);
                    notificationManager.cancel(activeCallNotificationId);
                }
                resetUI();
            }
        };
    }


    private void answer() {
        activeCallInvite.accept(this, callListener);
        notificationManager.cancel(activeCallNotificationId);
    }

    private void disconnect() {
        if (activeCall != null) {
            activeCall.disconnect();
            activeCall = null;
        }
    }

    private void setAudioFocus(boolean setFocus) {
        if (audioManager != null) {
            if (setFocus) {
                savedAudioMode = audioManager.getMode();
                // Request audio focus before making any device switch.
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    AudioAttributes playbackAttributes = new AudioAttributes.Builder()
                            .setUsage(AudioAttributes.USAGE_VOICE_COMMUNICATION)
                            .setContentType(AudioAttributes.CONTENT_TYPE_SPEECH)
                            .build();
                    AudioFocusRequest focusRequest = new AudioFocusRequest.Builder(AudioManager.AUDIOFOCUS_GAIN_TRANSIENT)
                            .setAudioAttributes(playbackAttributes)
                            .setAcceptsDelayedFocusGain(true)
                            .setOnAudioFocusChangeListener(new AudioManager.OnAudioFocusChangeListener() {
                                @Override
                                public void onAudioFocusChange(int i) {
                                }
                            })
                            .build();
                    audioManager.requestAudioFocus(focusRequest);
                }
                audioManager.setMode(AudioManager.MODE_IN_COMMUNICATION);
            } else {
                audioManager.setMode(savedAudioMode);
                audioManager.abandonAudioFocus(null);
            }
        }
    }

    private boolean checkPermissionForMicrophone() {
        int resultMic = ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO);
        return resultMic == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermissionForMicrophone() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.RECORD_AUDIO)) {
            Toast.makeText(VoiceActivity.this,
                    "Microphone permissions needed. Please allow in your application settings.",
                    Toast.LENGTH_SHORT).show();
        } else {
            ActivityCompat.requestPermissions(
                    this,
                    new String[]{Manifest.permission.RECORD_AUDIO},
                    MIC_PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == MIC_PERMISSION_REQUEST_CODE && permissions.length > 0) {
            if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(VoiceActivity.this,
                        "Microphone permissions needed. Please allow in your application settings.",
                        Toast.LENGTH_SHORT).show();
            } else {
                registerForCallInvites();
            }
        }
    }

    @Override
    public void onBackPressed() {
    }
}
