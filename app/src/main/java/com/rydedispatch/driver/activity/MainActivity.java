package com.rydedispatch.driver.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentManager;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.iid.FirebaseInstanceId;
import com.rydedispatch.driver.R;
import com.rydedispatch.driver.app.MyApp;
import com.rydedispatch.driver.fragment.EarningFragment;
import com.rydedispatch.driver.fragment.HistoryFragment;
import com.rydedispatch.driver.fragment.SettingFragment;
import com.rydedispatch.driver.utility.AppConfig;
import com.rydedispatch.driver.utility.PreferenceHelper;
import com.squareup.picasso.Picasso;
import com.twilio.voice.RegistrationException;
import com.twilio.voice.UnregistrationListener;
import com.twilio.voice.Voice;

import java.io.IOException;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private PreferenceHelper helper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        helper = new PreferenceHelper(this);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        View header = navigationView.getHeaderView(0);
        CircleImageView profileImg = header.findViewById(R.id.profile_image);
        TextView textView = header.findViewById(R.id.tv_driver_name);
        TextView tvEmail = header.findViewById(R.id.tv_email);
        textView.setText(helper.getFirstName() + " " + helper.getLastName());
        tvEmail.setText(helper.getEmail());

        Picasso.get().load(AppConfig.getImageUrl(helper.getUserPic())).
                placeholder(R.drawable.dummy_pic).
                error(R.drawable.dummy_pic).
                into(profileImg);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.nav_home) {
            FragmentManager fragmentManager = getSupportFragmentManager();
//            fragmentManager.beginTransaction()
//                    .replace(R.id.content_frame, new HomeFragment()).commit();
        } else if (id == R.id.nav_settings) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.content_frame, new SettingFragment()).commit();
        } else if (id == R.id.nav_change_duty) {
            startActivity(new Intent(getApplicationContext(), ChangeDutyActivity.class));
        } else if (id == R.id.nav_history) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.content_frame, new HistoryFragment()).commit();
        }else if (id == R.id.nav_earning) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.content_frame, new EarningFragment()).commit();
        } else if (id == R.id.nav_pre_schedule) {
            FragmentManager fragmentManager = getSupportFragmentManager();
//            fragmentManager.beginTransaction()
//                    .replace(R.id.content_frame, new PaymentFragment()).commit();
        } else if (id == R.id.nav_report_issue) {
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"info@rydedispatch.com"});
            i.putExtra(Intent.EXTRA_SUBJECT, MainActivity.this.getResources().getString(R.string.report_issue));
            i.putExtra(Intent.EXTRA_TEXT   , "body of email");
            try {
                startActivity(Intent.createChooser(i, "" + MainActivity.this.getResources().getString(R.string.send_email)));
            } catch (android.content.ActivityNotFoundException ex) {
                Toast.makeText(MainActivity.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.nav_customer_support) {
            startActivity(new Intent(getApplicationContext(), SupportActivity.class));
        } else if (id == R.id.nav_tnc) {
            startActivity(new Intent(getApplicationContext(), PrivacyPolicyActivity.class).putExtra("url", MyApp.getInstance().BASE_IMAGE_URL + "termsandCondition"));
        } else if (id == R.id.nav_logout) {
            logoutDialog();
        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void logoutDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setMessage(R.string.logout_msg);
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                final String fcmToken = FirebaseInstanceId.getInstance().getToken();
                Voice.unregister(helper.getTwilioAuth(), Voice.RegistrationChannel.FCM, fcmToken, new UnregistrationListener() {
                    @Override
                    public void onUnregistered(String accessToken, String fcmToken) {
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    FirebaseInstanceId.getInstance().deleteInstanceId();

                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }).start();
                        helper.clearAllPrefs();
                        startActivity(new Intent(MainActivity.this, LoginActivity.class));
                        finish();
                    }

                    @Override
                    public void onError(RegistrationException registrationException, String accessToken, String fcmToken) {

                    }
                });

                helper.clearAllPrefs();
                startActivity(new Intent(MainActivity.this, LoginActivity.class));
                finish();
            }
        });
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }
}
