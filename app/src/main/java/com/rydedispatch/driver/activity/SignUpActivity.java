package com.rydedispatch.driver.activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.hbb20.CountryCodePicker;
import com.rydedispatch.driver.R;
import com.rydedispatch.driver.app.MyApp;
import com.rydedispatch.driver.asynctask.ParamsRequest;
import com.rydedispatch.driver.utility.GPSTracker;
import com.rydedispatch.driver.utility.PreferenceHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import static com.rydedispatch.driver.utility.AppConfig.URL_CREATE_DRIVER;
import static com.rydedispatch.driver.utility.AppConfig.URL_SEND_OTP;


public class SignUpActivity extends AppCompatActivity {
    private EditText et_firstName;
    private EditText et_lastName;
    private EditText et_email;
    private EditText et_phNo;
    private CountryCodePicker countryCodePicker;
    private String FirstName;
    private String LastName;
    private String EmailId;
    private String PhoneNO;
    private String CountryCode;
    private String fcmToken;
    private PreferenceHelper helper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        et_firstName = findViewById(R.id.et_firstName);
        et_lastName = findViewById(R.id.et_lastName);
        et_email = findViewById(R.id.et_email);
        et_phNo = findViewById(R.id.et_phNo);
        countryCodePicker = findViewById(R.id.ccp);
        TextView txt_user = findViewById(R.id.txtUserPolicy);
        Button btn_signUp = findViewById(R.id.btn_signUp);
        Button btnClose = findViewById(R.id.btnClose);
        GPSTracker gps = new GPSTracker(SignUpActivity.this);
        helper = new PreferenceHelper(this);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btn_signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signUp();
            }
        });

        txt_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), PrivacyPolicyActivity.class).putExtra("url", MyApp.getInstance().BASE_IMAGE_URL+"userAccept"));
            }
        });

        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            return;
                        }
                        fcmToken = task.getResult().getToken();
                        Log.e("Token", fcmToken);
                    }
                });
    }
    private void signUp() {
        FirstName = et_firstName.getText().toString();
        LastName = et_lastName.getText().toString();
        EmailId = et_email.getText().toString();
        PhoneNO = et_phNo.getText().toString();
        CountryCode = countryCodePicker.getSelectedCountryCode();
        if (FirstName.isEmpty()) {
            et_firstName.setError("Enter the First Name");
            et_firstName.requestFocus();
            return;
        }
        if (LastName.isEmpty()) {
            et_lastName.setError("Enter the Last Name");
            et_lastName.requestFocus();
            return;
        }
        if (EmailId.isEmpty()) {
            et_email.setError("Enter the Email");
            et_email.requestFocus();
            return;
        }
        if (PhoneNO.isEmpty()) {
            et_phNo.setError("Enter the Phone No");
            et_phNo.requestFocus();
            return;
        }
        if (!validEmail()) {
            return;
        }
        sendOTP(0);
    }
    private boolean validEmail() {
        String email = et_email.getText().toString().trim();
        if (email.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            et_email.setError("Please Enter Valid Email ID");
            return false;
        } else {
            return true;
        }
    }
    private void sendOTP(final int flag) {
        HashMap<String, String> params = new HashMap<>();
        params.put("phone_number", PhoneNO);
        params.put("country_code", CountryCode);
        params.put("email", EmailId);


        ParamsRequest paramsRequest = new ParamsRequest(this, URL_SEND_OTP, params, new ParamsRequest.Callbacks() {
            @Override
            public void onFail(String error) {
                Toast.makeText(getApplicationContext(),error, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onSuccess(JSONObject response) {
                if(flag == 0){
                    OTPDialog();
                }
            }
        });

        paramsRequest.doInBackEnd();
    }

    private void OTPDialog(){
        View view = this.getLayoutInflater().inflate(R.layout.dialog_otp, null);
        final Dialog dialog = new Dialog(this, R.style.MaterialDialogSheet);
        dialog.setContentView(view);
        dialog.setCancelable(true);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setGravity(Gravity.CENTER);
        ImageButton ibCancel = view.findViewById(R.id.ib_close);
        Button btnSubmit = view.findViewById(R.id.btn_submit);
        Button btnResend = view.findViewById(R.id.btn_resend);
        final EditText etOtp = view.findViewById(R.id.et_otp);
        ibCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        btnResend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendOTP(1);
            }
        });
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String otp = etOtp.getText().toString();
                if (otp.isEmpty()) {
                    etOtp.setError("Enter the OTP");
                    etOtp.requestFocus();
                    return;
                }
                dialog.dismiss();
                createRegister(otp);
            }
        });
        dialog.show();
    }
    private void createRegister(String otp){

        HashMap<String, String> params = new HashMap<>();
        params.put("first_name", FirstName);
        params.put("last_name", LastName);
        params.put("email", EmailId);
        params.put("phone_number", PhoneNO);
        params.put("country_code", CountryCode);
        params.put("otp", otp);
        params.put("device_token", fcmToken);
        params.put("device_type", "android");
        params.put("driver_type", "android");
        params.put("company", "android");

        ParamsRequest paramsRequest = new ParamsRequest(SignUpActivity.this, URL_CREATE_DRIVER, params, new ParamsRequest.Callbacks() {
            @Override
            public void onFail(String error) {
                Toast.makeText(getApplicationContext(),error, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onSuccess(JSONObject response) {
                try {
                    JSONObject data = response.getJSONObject("data");
                    JSONObject userDetails = data.getJSONObject("userDetails");
                    if(!userDetails.isNull("profile_picture")){
                        helper.setUserPic(userDetails.getString("profile_picture"));
                    }
                    helper.setFirstName(userDetails.getString("first_name"));
                    helper.setLastName(userDetails.getString("last_name"));
                    helper.setAuth(userDetails.getString("token"));
                    helper.setUserUniqueCode(userDetails.getString("user_unique_code"));
                    helper.setLogin(true);

                    startActivity(new Intent(SignUpActivity.this,MainActivity.class));
                    finish();
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.userRegister), Toast.LENGTH_SHORT).show();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        paramsRequest.doInBackEnd();
    }
}
