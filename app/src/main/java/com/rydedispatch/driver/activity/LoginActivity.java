package com.rydedispatch.driver.activity;

import android.Manifest;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.hbb20.CountryCodePicker;
import com.rydedispatch.driver.R;
import com.rydedispatch.driver.asynctask.ParamsRequest;
import com.rydedispatch.driver.utility.PreferenceHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

import static com.rydedispatch.driver.utility.AppConfig.URL_LOGIN;
import static com.rydedispatch.driver.utility.AppConfig.URL_SEND_LOGIN_OTP;

public class LoginActivity extends AppCompatActivity {
    private CountryCodePicker countryCodePicker;
    private EditText edt_phone_login;
    private String phoneNo;
    private String countryCode;
    private EditText etOtp;
    private String fcmToken;
    private PreferenceHelper helper;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        helper = new PreferenceHelper(this);
        countryCodePicker = findViewById(R.id.ccp);
        edt_phone_login = findViewById(R.id.edt_phone_login);
        Button btn_login = findViewById(R.id.btn_login);
        Button btn_signUp = findViewById(R.id.btn_signUp);
        PermissionListener permissionlistener = new PermissionListener() {
            @Override
            public void onPermissionGranted() {

            }
            @Override
            public void onPermissionDenied(List<String> deniedPermissions) {
                Toast.makeText(LoginActivity.this, "Permission Denied\n" + deniedPermissions.toString(), Toast.LENGTH_SHORT).show();
            }
        };
        TedPermission.with(this)
                .setPermissionListener(permissionlistener)
                .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                .setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.RECORD_AUDIO)
                .check();
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            return;
                        }
                        fcmToken = task.getResult().getToken();
                        Log.e("onComplete: " , fcmToken);
                    }
                });
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!validatePhone()){
                    return;
                }
                countryCode = countryCodePicker.getSelectedCountryCode();
                phoneNo = edt_phone_login.getText().toString();
                SendOTP(0);
            }
        });
        btn_signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this,SignUpActivity.class));
            }
        });
    }
    private boolean validatePhone() {
        if (edt_phone_login.getText().toString().trim().isEmpty()) {
            edt_phone_login.requestFocus();
            Toast.makeText(getApplicationContext(),"Enter Phone no", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void SendOTP(final int flag){

        HashMap<String, String> params = new HashMap<>();
        params.put("phone_number", phoneNo);
        params.put("country_code",countryCode);

        ParamsRequest paramsRequest = new ParamsRequest(LoginActivity.this, URL_SEND_LOGIN_OTP, params, new ParamsRequest.Callbacks() {
            @Override
            public void onFail(String error) {
                Toast.makeText(getApplicationContext(),error, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onSuccess(JSONObject response) {
                if(flag == 0){
                    OTPDialog();
                }
            }
        });

        paramsRequest.doInBackEnd();
    }
    private void OTPDialog(){
        View view = this.getLayoutInflater().inflate(R.layout.dialog_otp, null);
        final Dialog dialog = new Dialog(this, R.style.MaterialDialogSheet);
        dialog.setContentView(view);
        dialog.setCancelable(true);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setGravity(Gravity.CENTER);
        ImageButton ibCancel = view.findViewById(R.id.ib_close);
        Button btnSubmit = view.findViewById(R.id.btn_submit);
        Button btnResend = view.findViewById(R.id.btn_resend);
        etOtp = view.findViewById(R.id.et_otp);
        ibCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        btnResend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SendOTP(1);
            }
        });
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String otp = etOtp.getText().toString();
                if (otp.isEmpty()) {
                        etOtp.setError("Enter the OTP");
                        etOtp.requestFocus();
                        return;
                }
                loginUser(phoneNo, countryCode, otp, fcmToken);
            }
        });
        dialog.show();
    }

    private void loginUser(String phoneNo, String countryCode, String otp, String fcmToken) {

        HashMap<String, String> params = new HashMap<>();
        params.put("phone_number", phoneNo);
        params.put("country_code",countryCode);
        params.put("otp",otp);
        params.put("device_token",fcmToken);
        params.put("device_type","android");

        ParamsRequest paramsRequest = new ParamsRequest(LoginActivity.this, URL_LOGIN, params, new ParamsRequest.Callbacks() {
            @Override
            public void onFail(String error) {
                Toast.makeText(getApplicationContext(),error, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onSuccess(JSONObject response) {
                try {
                    JSONObject data = response.getJSONObject("data");
                    JSONObject userDetail = data.getJSONObject("userDetails");
                    if(userDetail.getString("role").equalsIgnoreCase("driver")){
                        helper.setFirstName(userDetail.getString("first_name"));
                        helper.setLastName(userDetail.getString("last_name"));
                        helper.setUserPic(userDetail.getString("profile_picture"));
                        helper.setEmail(userDetail.getString("email"));
                        helper.setAuth(userDetail.getString("token"));
                        helper.setUserUniqueCode(userDetail.getString("user_unique_code"));
                        helper.setUserStatus(userDetail.getString("user_status"));
                        helper.setPhoneNumber(userDetail.getString("phone_number"));
                        helper.setLogin(true);
                        startActivity(new Intent(LoginActivity.this,MainActivity.class));
                        finish();
                    }else{
                        Alert();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        paramsRequest.doInBackEnd();
    }

    private void Alert(){
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Alert")
                .setMessage("This account does not have proper role to login please login with other details.")
                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                         finish();
                    }
                })
                .show();
    }
}
