package com.rydedispatch.driver.activity;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.rydedispatch.driver.R;
import com.rydedispatch.driver.adpater.ChatHistoryAdapter;
import com.rydedispatch.driver.asynctask.ParamsRequest;
import com.rydedispatch.driver.model.Message;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import static com.rydedispatch.driver.utility.AppConfig.URL_GET_MESSAGE;
import static com.rydedispatch.driver.utility.AppConfig.URL_SEND_MESSAGE;

public class SupportChatActivity extends AppCompatActivity {

    private int currentPage = 1;
    private int lastPage = 1;
    private EditText et_textMessage;
    private ChatHistoryAdapter chatHistoryAdapter;
    private ArrayList<Message> messageArrayList = new ArrayList<>() ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_support_chat);

        if(getSupportActionBar()!=null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        RecyclerView rvChat = findViewById(R.id.rv_chat);
        et_textMessage = findViewById(R.id.et_textMessage);
        Button btn_send = findViewById(R.id.btn_send);

        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!et_textMessage.getText().toString().isEmpty()){
                    sendMessage(et_textMessage.getText().toString());
                }else{
                    Toast.makeText(getApplicationContext(),getString(R.string.error_send_msg), Toast.LENGTH_LONG).show();
                }

            }
        });

        rvChat.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,true));
        rvChat.setItemAnimator(new DefaultItemAnimator());
        rvChat.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (currentPage == lastPage) {
                    return;
                }
                getMessages(++currentPage);
            }
        });

        chatHistoryAdapter = new ChatHistoryAdapter(messageArrayList);
        rvChat.setAdapter(chatHistoryAdapter);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        getMessages(currentPage);
    }

    private void getMessages(int page){
        if(page==1){
            messageArrayList.clear();
        }

        HashMap<String, String> params = new HashMap<>();
        params.put("page", String.valueOf(page));

        ParamsRequest paramsRequest = new ParamsRequest(SupportChatActivity.this, URL_GET_MESSAGE, params, new ParamsRequest.Callbacks() {
            @Override
            public void onFail(String error) {
                Toast.makeText(getApplicationContext(),error, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onSuccess(JSONObject response) {
                try {
                    JSONObject data = response.getJSONObject("data");
                    JSONObject history = data.getJSONObject("chats");
                    lastPage = history.getInt("last_page");
                    JSONArray jsonArray = history.getJSONArray("data");
                    for (int i = 0;i<jsonArray.length();i++){
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String message = jsonObject.getString("message");
                        int direction = jsonObject.getInt("direction") ;
                        String is_read = jsonObject.getString("is_read");
                        String updated_at = jsonObject.getString("updated_at");
                        String first_name = jsonObject.getString("first_name");
                        String last_name = jsonObject.getString("last_name");
                        String profile_picture = jsonObject.getString("profile_picture");
                        messageArrayList.add(new Message(message,direction,is_read,updated_at,first_name,last_name,profile_picture));
                    }

                    chatHistoryAdapter.notifyDataSetChanged();

                }catch (JSONException e){
                    e.printStackTrace();
                }
            }
        });
        paramsRequest.doInBackEnd();
    }

    private void sendMessage(String msg){
        HashMap<String, String> params = new HashMap<>();
        params.put("message",msg);

        ParamsRequest paramsRequest = new ParamsRequest(SupportChatActivity.this, URL_SEND_MESSAGE, params, new ParamsRequest.Callbacks() {
            @Override
            public void onFail(String error) {
                Toast.makeText(getApplicationContext(),error, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onSuccess(JSONObject response) {
                et_textMessage.setText("");
                getMessages(currentPage);
            }
        });
        paramsRequest.doInBackEnd();

    }
}
