package com.rydedispatch.driver.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import com.rydedispatch.driver.R;
import com.rydedispatch.driver.asynctask.ParamsRequest;
import com.rydedispatch.driver.utility.PreferenceHelper;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;

import static com.rydedispatch.driver.utility.AppConfig.URL_CHANGE_STATUS;

public class ChangeDutyActivity extends AppCompatActivity {

    private PreferenceHelper helper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_duty);

        helper = new PreferenceHelper(this);
        ImageView ivDriver = findViewById(R.id.iv_driver);
        TextView tvOnDuty = findViewById(R.id.tv_on_duty);
        TextView tvStatus = findViewById(R.id.tv_status);
        Button btnChangeStatus = findViewById(R.id.btn_change_status);

        if(getSupportActionBar()!=null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        tvStatus.setText(helper.getUserStatus());

        if(helper.getUserStatus().equalsIgnoreCase("online")){
            ivDriver.setBackground(getResources().getDrawable(R.drawable.online_driver));
            tvOnDuty.setTextColor(getResources().getColor(R.color.green1));
            btnChangeStatus.setText(R.string.go_offline);
        }else{
            ivDriver.setBackground(getResources().getDrawable(R.drawable.offline_driver));
            tvOnDuty.setTextColor(getResources().getColor(R.color.icons_8_muted_red));
            btnChangeStatus.setText(R.string.go_online);
        }

        btnChangeStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (helper.getUserStatus().equalsIgnoreCase("online")) {
                    changeStatus("offline");
                } else {
                    changeStatus("online");
                }
            }
        });

    }

    private void changeStatus(final String status){
        HashMap<String, String> params = new HashMap<>();
        params.put("user_status", status);

        ParamsRequest paramsRequest = new ParamsRequest(ChangeDutyActivity.this, URL_CHANGE_STATUS, params, new ParamsRequest.Callbacks() {
            @Override
            public void onFail(String error) {
                Toast.makeText(getApplicationContext(),error, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onSuccess(JSONObject response) {
                try {
                    helper.setUserStatus(status);
                    Toast.makeText(ChangeDutyActivity.this, response.getString("message"), Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(ChangeDutyActivity.this, MainActivity.class));

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        paramsRequest.doInBackEnd();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return true;
    }

}
