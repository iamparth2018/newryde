package com.rydedispatch.driver.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.firebase.iid.FirebaseInstanceId;
import com.rydedispatch.driver.R;
import com.rydedispatch.driver.app.MyApp;
import com.rydedispatch.driver.asynctask.SimpleRequest;
import com.rydedispatch.driver.asynctask.VolleyMultipartRequest;
import com.rydedispatch.driver.utility.AppConfig;
import com.rydedispatch.driver.utility.PreferenceHelper;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.rydedispatch.driver.utility.AppConfig.URL_DRIVER_DASHBOARD;
import static com.rydedispatch.driver.utility.AppConfig.URL_UPDATE_PROFILE;

public class MyProfileActivity extends AppCompatActivity {

    private PreferenceHelper helper;
    private int SELECT_FILE = 212;
    private int REQUEST_CAMERA = 213;
    private File profilePic;
    private CircleImageView img_profile;
    private EditText etFirstName,etLastName, etEmail, etPhone;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);
        helper = new PreferenceHelper(this);

        if(getSupportActionBar()!=null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        etFirstName = findViewById(R.id.et_first_name);
        etLastName = findViewById(R.id.et_last_name);
        etEmail = findViewById(R.id.et_email);
        etPhone = findViewById(R.id.et_phone);
        ImageView ibEdit = findViewById(R.id.ib_edit);
        Button btnSave = findViewById(R.id.btn_save);
        img_profile = findViewById(R.id.profile_image);

        etFirstName.setText(helper.getFirstName());
        etLastName.setText(helper.getLastName());
        etEmail.setText(helper.getEmail());
        etPhone.setText(helper.getPhoneNumber());
        Picasso.get().load(AppConfig.getImageUrl(helper.getUserPic())).
                placeholder(R.drawable.dummy_pic).
                error(R.drawable.dummy_pic).
                into(img_profile);

        ibEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etFirstName.getText().toString().isEmpty()) {
                    etFirstName.setError("Enter the First Name");
                    etFirstName.requestFocus();
                    return;
                }
                if (etLastName.getText().toString().isEmpty()) {
                    etLastName.setError("Enter the Last Name");
                    etLastName.requestFocus();
                    return;
                }
                if (etEmail.getText().toString().isEmpty()) {
                    etEmail.setError("Enter the Email");
                    etEmail.requestFocus();
                    return;
                }
                if (!validEmail()) {
                    return;
                }
                uploadImage(etFirstName.getText().toString(), etLastName.getText().toString(), etEmail.getText().toString());
            }
        });

        getProfile();
    }

    private boolean validEmail() {
        String email = etEmail.getText().toString().trim();
        if (email.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            etEmail.setError("Please Enter Valid Email ID");
            return false;
        } else {
            return true;
        }
    }

    private void getProfile(){
        SimpleRequest paramsRequest = new SimpleRequest(MyProfileActivity.this, URL_DRIVER_DASHBOARD, new SimpleRequest.Callbacks() {
            @Override
            public void onFail(String error) {
                Toast.makeText(getApplicationContext(),error, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onSuccess(JSONObject response) {
                try {
                    JSONObject data = response.getJSONObject("data");
                    JSONObject userDetail = data.getJSONObject("userDetails");
                    helper.setFirstName(userDetail.getString("first_name"));
                    helper.setLastName(userDetail.getString("last_name"));
                    helper.setUserPic(userDetail.getString("profile_picture"));
                    helper.setEmail(userDetail.getString("email"));
                    helper.setUserStatus(userDetail.getString("user_status"));
                    helper.setPhoneNumber(userDetail.getString("phone_number"));

                    etFirstName.setText(helper.getFirstName());
                    etLastName.setText(helper.getLastName());
                    etEmail.setText(helper.getEmail());
                    etPhone.setText(helper.getPhoneNumber());

                    Picasso.get().load(AppConfig.getImageUrl(helper.getUserPic())).
                            placeholder(R.drawable.dummy_pic).
                            error(R.drawable.dummy_pic).
                            into(img_profile);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        paramsRequest.doInBackEnd();
    }

    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    cameraIntent();
                } else if (items[item].equals("Choose from Library")) {
                    galleryIntent();
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }
    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select File"),SELECT_FILE);
    }
    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                profilePic = onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                profilePic = onCaptureImageResult(data);
        }
    }
    private File onSelectFromGalleryResult(Intent data) {
        Bitmap bm=null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getContentResolver(), data.getData());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        img_profile.setImageBitmap(bm);
        return saveImage(bm);
    }
    private File onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File path = new File(String.valueOf(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)));
        if (!path.exists()) {
            path.mkdirs();
        }
        FileOutputStream fo;
        try {
            File destination = new File(path, System.currentTimeMillis() + ".jpeg");
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
            img_profile.setImageBitmap(thumbnail);
            return destination;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    private File saveBitmapToFile(File file){
        try {
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            o.inSampleSize = 6;
            FileInputStream inputStream = new FileInputStream(file);
            BitmapFactory.decodeStream(inputStream, null, o);
            inputStream.close();
            final int REQUIRED_SIZE=75;
            int scale = 1;
            while(o.outWidth / scale / 2 >= REQUIRED_SIZE &&
                    o.outHeight / scale / 2 >= REQUIRED_SIZE) {
                scale *= 2;
            }
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            inputStream = new FileInputStream(file);
            Bitmap selectedBitmap = BitmapFactory.decodeStream(inputStream, null, o2);
            inputStream.close();
            file.createNewFile();
            FileOutputStream outputStream = new FileOutputStream(file);
            selectedBitmap.compress(Bitmap.CompressFormat.JPEG, 100 , outputStream);
            return file;
        } catch (Exception e) {
            return null;
        }
    }
    private File saveImage(Bitmap myBitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File wallpaperDirectory = new File(String.valueOf(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)));
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs();
        }
        try {
            File f = new File(wallpaperDirectory, Calendar.getInstance()
                    .getTimeInMillis() + ".jpeg");
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            fo.close();
            return saveBitmapToFile(f);
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return null;
    }

    private void uploadImage(final String firstName, final String lastName, final String emailId){

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(getResources().getString(R.string.pls_wait));
        progressDialog.show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, MyApp.getInstance().BASE_URL+URL_UPDATE_PROFILE,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
                        if(!(MyProfileActivity.this.isDestroyed() && !MyProfileActivity.this.isFinishing())){
                            if (progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }
                        }
                        try {
                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            if(!jsonObject.getBoolean("error")){
                                Log.e("OnResponse ", new String(response.data));
//                                getProfile(1);
                            }else{
                                if(jsonObject.getString("message").equalsIgnoreCase("Unauthenticated")){
                                    try {
                                        FirebaseInstanceId.getInstance().deleteInstanceId();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                    logoutDialog(MyProfileActivity.this);
                                }else{
                                    Toast.makeText(MyProfileActivity.this,jsonObject.getString("message"),Toast.LENGTH_LONG).show();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(!(MyProfileActivity.this.isDestroyed() && !MyProfileActivity.this.isFinishing())){
                            if (progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }
                        }
                        Log.e("onError ", "error"+ error.getMessage());
                        Toast.makeText(MyProfileActivity.this,error.getMessage(),Toast.LENGTH_LONG).show();
                    }
                })

        {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Accept","application/json");
                headers.put("Authorization", "Bearer "+helper.getAuth());
                return headers;
            }

            @Override
            protected Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<>();
                params.put("first_name", firstName);
                params.put("last_name",lastName);
                params.put("email",emailId);
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                if(profilePic!=null){
                    params.put("profile_picture", new DataPart(profilePic.getName(), getFileDataFromDrawable(BitmapFactory.decodeFile(profilePic.getAbsolutePath()))));
                }
                return params;
            }
        };

        MyApp.getInstance().addToRequestQueue(volleyMultipartRequest, "string_req");
    }

    private byte[] getFileDataFromDrawable(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 80, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    private void logoutDialog(final Context context){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(R.string.auth_title);
        builder.setMessage(R.string.auth_msg);
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                helper.clearAllPrefs();
                context.startActivity(new Intent(context, LoginActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
            }
        });
        builder.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return true;
    }


}
