package com.rydedispatch.driver.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;

import com.rydedispatch.driver.R;
import com.rydedispatch.driver.asynctask.ParamsRequest;
import com.rydedispatch.driver.utility.AppConfig;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.rydedispatch.driver.utility.AppConfig.URL_HISTORY_DETAILS;

public class HistoryDetailActivity extends AppCompatActivity {

    @BindView(R.id.tv_passenger)
    AppCompatTextView tv_passenger;
    @BindView(R.id.rating_user)
    RatingBar ratinguser;
    @BindView(R.id.tv_total_time)
    AppCompatTextView tv_total_time;
    @BindView(R.id.tv_miles)
    AppCompatTextView tv_miles;
    @BindView(R.id.tv_booking_date)
    AppCompatTextView tvbookingtime;
    @BindView(R.id.tv_source_loc)
    AppCompatTextView tv_source_loc;
    @BindView(R.id.tv_destination_loc)
    AppCompatTextView tv_destination_loc;
    @BindView(R.id.tv_start_riding_time)
    AppCompatTextView tv_start_riding_time;
    @BindView(R.id.tv_completed_time)
    AppCompatTextView tv_completed_time;
    @BindView(R.id.tv_total_distance)
    AppCompatTextView tv_total_distance;
    @BindView(R.id.tv_base_fare)
    AppCompatTextView tv_base_fare;
    @BindView(R.id.tv_extra_ride_time_cost)
    AppCompatTextView tv_extra_ride_time_cost;
    @BindView(R.id.tv_total_cost)
    AppCompatTextView tv_total_cost;
    @BindView(R.id.tv_discount_value)
    AppCompatTextView tv_discount_value;
    @BindView(R.id.tv_sub_total_value)
    AppCompatTextView tv_sub_total_value;
    @BindView(R.id.tv_extra_distance_cost)
    AppCompatTextView tv_extra_distance_cost;
    @BindView(R.id.tv_wait_time_cost)
    AppCompatTextView tv_wait_time_cost;
    @BindView(R.id.tv_estimated_cost_without_base_fare)
    AppCompatTextView tv_estimated_cost_without_base_fare;
    @BindView(R.id.tv_status)
    AppCompatTextView tv_status;
    @BindView(R.id.tv_display_name_detail)
    AppCompatTextView tvdisplayname;
    @BindView(R.id.tv_commission_value)
    AppCompatTextView tv_commission_value;
    @BindView(R.id.tv_bonus_value)
    AppCompatTextView tv_bonus_value;
    @BindView(R.id.ll_bid_details)
    LinearLayout ll_bid_details;
    @BindView(R.id.tv_time_detail)
    AppCompatTextView tv_time_detail;
    @BindView(R.id.tv_est_to_trip)
    AppCompatTextView tv_est_to_trip;
    @BindView(R.id.tv_time_est_driver)
    AppCompatTextView tv_time_est_driver;
    @BindView(R.id.ll_schedule_trip)
    LinearLayout ll_schedule_trip;
    @BindView(R.id.ll_navigate_schedule)
    LinearLayout ll_navigate_schedule;
    @BindView(R.id.ll_call_schedule)
    LinearLayout ll_call_schedule;
    @BindView(R.id.tv_other_details)
    AppCompatTextView tv_other_details;
    @BindView(R.id.ll_navigate_call)
    LinearLayout ll_navigate_call;
    @BindView(R.id.profile_image_details)
    CircleImageView profile_image;
    @BindView(R.id.ll_detail)
    LinearLayout ll_detail;

    int request_id;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_detail);

        if(getSupportActionBar()!=null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            request_id = bundle.getInt(AppConfig.EXTRA.requestId);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        getHistoryDetail();
    }

    public void getHistoryDetail(){
        HashMap<String, String> params = new HashMap<>();
        params.put("request_id", String.valueOf(request_id));

        ParamsRequest paramsRequest = new ParamsRequest(HistoryDetailActivity.this, URL_HISTORY_DETAILS, params, new ParamsRequest.Callbacks() {
            @Override
            public void onFail(String error) {
                Toast.makeText(getApplicationContext(),error, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onSuccess(JSONObject response) {
                /*try {
                    JSONObject data = response.getJSONObject("data");
                    passengername = data.getString("passenger_first_name");
                    ratinguserstr = Float.parseFloat(data.getString("user_rating"));
                    totaltime = data.getString("total_time");
                    distance = data.getString("total_distance");
                    km = data.getString("distance_type");
                    bookingTime = data.getString("");
                    source_loc = data.getString("source_location");
                    destination_loc = data.getString("destination_location");
                    startridingtime = data.getString("start_riding_time");
                    completed_time = data.getString("completed_time");
                    base_fare = data.getString("base_fare");
                    extra_ride_time_cost = data.getString("extra_ride_time_cost");
                    estimated_cost_without_base_fare = data.getString("estimated_cost_without_base_fare");
                    status = data.getString("status");
                    subuser = data.getString("sub_user_id");
                    displayname = data.getString("display_passenger_name");
                    estimatedtimetoarrival = data.getString("estimated_time_to_arrival");
                    payment_type = data.getString("payment_type");
                } catch (JSONException e) {
                    e.printStackTrace();
                }*/
            }
        });

        paramsRequest.doInBackEnd();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return true;
    }

}
