package com.rydedispatch.driver.service;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.service.notification.StatusBarNotification;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.rydedispatch.driver.R;
import com.rydedispatch.driver.activity.MainActivity;
import com.rydedispatch.driver.activity.VoiceActivity;
import com.twilio.voice.CallInvite;
import com.twilio.voice.CancelledCallInvite;
import com.twilio.voice.MessageListener;
import com.twilio.voice.Voice;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

public class NotifyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "FirebaseMessaging";
    private static final String NOTIFICATION_ID_KEY = "NOTIFICATION_ID";
    private static final String CALL_SID_KEY = "CALL_SID";
    private static final String VOICE_CHANNEL = "default";

    private NotificationManager notificationManager;

    @Override
    public void onCreate() {
        super.onCreate();
        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
    }


    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Log.d(TAG, "From: " + remoteMessage.getFrom());
        Log.d(TAG,"Remote message " + remoteMessage.getData());
        Log.d(TAG,"Remote message " + remoteMessage.getNotification());

//        if (remoteMessage.getData().size() > 0) {
//            Map<String, String> data = remoteMessage.getData();
//            final int notificationId = (int) System.currentTimeMillis();
//            String checkNotification = String.valueOf(data.get("main_title"));
//            Log.e(TAG, "onMessageReceived: " + checkNotification);
//            if (checkNotification.equalsIgnoreCase("null")) {
//                Voice.handleMessage(this, data, new MessageListener() {
//                    @Override
//                    public void onCallInvite(CallInvite callInvite) {
//                        NotifyFirebaseMessagingService.this.notify(callInvite, notificationId);
//                    }
//
//                    @Override
//                    public void onError(MessageException messageException) {
//                        Log.e(TAG, messageException.getLocalizedMessage());
//                    }
//                });
//            } else {
//                sendNotification(remoteMessage.getData());
//            }
//        }

        if (remoteMessage.getData().size() > 0) {
            boolean valid = Voice.handleMessage(remoteMessage.getData(), new MessageListener() {
                @Override
                public void onCallInvite(@NonNull CallInvite callInvite) {
                    final int notificationId = (int) System.currentTimeMillis();
                    NotifyFirebaseMessagingService.this.notify(callInvite, notificationId);
                    NotifyFirebaseMessagingService.this.sendCallInviteToActivity(callInvite, notificationId);
                }

                @Override
                public void onCancelledCallInvite(@NonNull CancelledCallInvite cancelledCallInvite) {
                    NotifyFirebaseMessagingService.this.cancelNotification(cancelledCallInvite);
                    NotifyFirebaseMessagingService.this.sendCancelledCallInviteToActivity(
                            cancelledCallInvite);
                }

            });

            if (!valid) {
                sendNotification(remoteMessage.getData());
                Log.e(TAG, "The message was not a valid Twilio Voice SDK payload: " +
                        remoteMessage.getData());
            }

        }

    }

    private void sendNotification(Map<String, String> data){

        String title = data.get("title");
        String main_title = data.get("main_title");
        String nestedBody = data.get("body");
        String rideReqId = data.get("request_id");

        if(rideReqId==null){
            try {
                JSONObject jsonObject = new JSONObject(data.get("data"));
                rideReqId = jsonObject.getString("request_id");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        Log.e(TAG, "sendNotification: " + rideReqId + " " + nestedBody + " " + main_title);


        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        if(main_title.equalsIgnoreCase("accepted_ride_request") || main_title.equalsIgnoreCase("driver_arrived") || main_title.equalsIgnoreCase("started_ride_request")) {
//            intent = new Intent(this, TrackRideActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("requestId", rideReqId);
            startActivity(intent);
        }else if(main_title.equalsIgnoreCase("completed_ride_request")){
//            intent = new Intent(this, RatingDriverActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("requestId", rideReqId);
            startActivity(intent);
        }else if(main_title.equalsIgnoreCase("ride_no_show") ||
                main_title.equalsIgnoreCase("schedule_accepted_ride_request") ||
                main_title.equalsIgnoreCase("no_driver_found") || main_title.equalsIgnoreCase("ride_cancel_by_driver")){

            intent = new Intent(this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }

        Uri defaultSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        Log.e(TAG, "sendNotification: " + intent.toString());

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_ONE_SHOT);

        String NOTIFICATION_CHANNEL_ID = "101";

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            @SuppressLint("WrongConstant")
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "Notification", NotificationManager.IMPORTANCE_MAX);

            notificationChannel.setDescription("Ryde Notifications");
            notificationChannel.enableLights(true);
            notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
            notificationChannel.setLightColor(getColor(R.color.colorPrimary));
            notificationChannel.enableVibration(true);
            notificationManager.createNotificationChannel(notificationChannel);
        }

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_notification)
                .setContentTitle(title)
                .setAutoCancel(true)
                .setOngoing(false)
                .setSound(defaultSound)
                .setColor(getResources().getColor(R.color.colorPrimary))
                .setContentText(nestedBody)
                .setContentIntent(pendingIntent)
                .setWhen(System.currentTimeMillis())
                .setPriority(Notification.PRIORITY_MAX);

        notificationManager.notify(1, notificationBuilder.build());
    }


    private void notify(CallInvite callInvite, int notificationId) {
        Intent intent = new Intent(this, VoiceActivity.class);
        intent.setAction(VoiceActivity.ACTION_INCOMING_CALL);
        intent.putExtra(VoiceActivity.INCOMING_CALL_NOTIFICATION_ID, notificationId);
        intent.putExtra(VoiceActivity.INCOMING_CALL_INVITE, callInvite);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent =
                PendingIntent.getActivity(this, notificationId, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        /*
         * Pass the notification id and call sid to use as an identifier to cancel the
         * notification later
         */
        Bundle extras = new Bundle();
        extras.putInt(NOTIFICATION_ID_KEY, notificationId);
        extras.putString(CALL_SID_KEY, callInvite.getCallSid());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel callInviteChannel = new NotificationChannel(VOICE_CHANNEL,
                    "Primary Voice Channel", NotificationManager.IMPORTANCE_DEFAULT);
            callInviteChannel.setLightColor(Color.GREEN);
            callInviteChannel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
            notificationManager.createNotificationChannel(callInviteChannel);

            Notification notification =
                    buildNotification(callInvite.getFrom() + " is calling.",
                            pendingIntent,
                            extras);
            notificationManager.notify(notificationId, notification);
        } else {
            NotificationCompat.Builder notificationBuilder =
                    new NotificationCompat.Builder(this)
                            .setSmallIcon(R.drawable.ic_call_end)
                            .setContentTitle(getString(R.string.app_name))
                            .setContentText(callInvite.getFrom() + " is calling.")
                            .setAutoCancel(true)
                            .setExtras(extras)
                            .setContentIntent(pendingIntent)
                            .setGroup("test_app_notification")
                            .setColor(Color.rgb(214, 10, 37));

            notificationManager.notify(notificationId, notificationBuilder.build());
        }
    }

    private void cancelNotification(CancelledCallInvite cancelledCallInvite) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            /*
             * If the incoming call was cancelled then remove the notification by matching
             * it with the call sid from the list of notifications in the notification drawer.
             */
            StatusBarNotification[] activeNotifications =
                    notificationManager.getActiveNotifications();
            for (StatusBarNotification statusBarNotification : activeNotifications) {
                Notification notification = statusBarNotification.getNotification();
                Bundle extras = notification.extras;
                String notificationCallSid = extras.getString(CALL_SID_KEY);

                if (cancelledCallInvite.getCallSid().equals(notificationCallSid)) {
                    notificationManager.cancel(extras.getInt(NOTIFICATION_ID_KEY));
                }
            }
        } else {
            /*
             * Prior to Android M the notification manager did not provide a list of
             * active notifications so we lazily clear all the notifications when
             * receiving a CancelledCallInvite.
             *
             * In order to properly cancel a notification using
             * NotificationManager.cancel(notificationId) we should store the call sid &
             * notification id of any incoming calls using shared preferences or some other form
             * of persistent storage.
             */
            notificationManager.cancelAll();
        }
    }

    /*
     * Send the CallInvite to the VoiceActivity. Start the activity if it is not running already.
     */
    private void sendCallInviteToActivity(CallInvite callInvite, int notificationId) {
        Intent intent = new Intent(this, VoiceActivity.class);
        intent.setAction(VoiceActivity.ACTION_INCOMING_CALL);
        intent.putExtra(VoiceActivity.INCOMING_CALL_NOTIFICATION_ID, notificationId);
        intent.putExtra(VoiceActivity.INCOMING_CALL_INVITE, callInvite);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        this.startActivity(intent);
    }

    /*
     * Send the CancelledCallInvite to the VoiceActivity
     */
    private void sendCancelledCallInviteToActivity(CancelledCallInvite cancelledCallInvite) {
        Intent intent = new Intent(VoiceActivity.ACTION_CANCEL_CALL);
        intent.putExtra(VoiceActivity.CANCELLED_CALL_INVITE, cancelledCallInvite);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    /**
     * Build a notification.
     *
     * @param text          the text of the notification
     * @param pendingIntent the body, pending intent for the notification
     * @param extras        extras passed with the notification
     * @return the builder
     */
    @TargetApi(Build.VERSION_CODES.O)
    private Notification buildNotification(String text, PendingIntent pendingIntent, Bundle extras) {
        return new Notification.Builder(getApplicationContext(), VOICE_CHANNEL)
                .setSmallIcon(R.drawable.ic_call_end)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(text)
                .setContentIntent(pendingIntent)
                .setExtras(extras)
                .setAutoCancel(true)
                .build();
    }

}
