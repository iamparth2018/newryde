package com.rydedispatch.driver.adpater;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.rydedispatch.driver.R;
import com.rydedispatch.driver.activity.HistoryDetailActivity;
import com.rydedispatch.driver.activity.SupportActivity;
import com.rydedispatch.driver.activity.VoiceActivity;
import com.rydedispatch.driver.model.History;
import com.rydedispatch.driver.utility.AppConfig;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

import static io.fabric.sdk.android.Fabric.TAG;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.ViewHolder> {

    private ArrayList<History> histories;
    private Context mContext;

    public HistoryAdapter(Context context,ArrayList<History> histories){
        this.histories = histories;
        this.mContext = context;
    }

    @NonNull
    @Override
    public HistoryAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_history, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final HistoryAdapter.ViewHolder holder, int position) {
        final History entity = histories.get(position);
        holder.tv_name.setText(entity.getFirstName());
        holder.tv_source_location.setText(entity.getSourceLocation());
        holder.tv_destination_location.setText(entity.getDestinationLocation());

        if (entity.getStatus().contains("_")) {
            holder.tv_status.setText(entity.getStatus().replace("_", " ").replace("_", " "));
        }
        else {
            holder.tv_status.setText(entity.getStatus());
        }


        if (entity.getStatus().equalsIgnoreCase("cancelled_by_driver")
                || entity.getStatus().equalsIgnoreCase("cancelled")
                || entity.getStatus().equalsIgnoreCase("no_show")) {
            holder.tv_status.setTextColor(mContext.getResources().getColor(R.color.red_background));
        }
        else {
            holder.tv_status.setTextColor(mContext.getResources().getColor(R.color.green));
        }

        if (entity.getStatus().equalsIgnoreCase("inprogress")
                || entity.getStatus().equalsIgnoreCase("pending")) {
            holder.image_navigate.setVisibility(View.VISIBLE);
            holder.image_call.setVisibility(View.VISIBLE);
        } else {
            holder.image_navigate.setVisibility(View.INVISIBLE);
            holder.image_call.setVisibility(View.INVISIBLE);
        }

        if (entity.getTotalCost() == null) {
            holder.tv_total_cost.setText("$ " + "0");

        }
        else {
            holder.tv_total_cost.setText("$ " +entity.getTotalCost());

        }


        if (entity.getScheduleDate()==null) {
            holder.tv_completed_time.setText(entity.getCompletedTime());
        }
        else {
            holder.tv_completed_time.setText(entity.getScheduleDate());

        }



        if (entity.getSub_user_id()>0) {

            holder.tv_display_name.setVisibility(View.VISIBLE);
            holder.tv_display_name.setText(entity.getDisplay_passenger_name());

        }
        else {
            holder.tv_display_name.setVisibility(View.INVISIBLE);

        }

        Picasso.get().load(AppConfig.getImageUrl(entity.getProfilePicture())).
                placeholder(R.drawable.dummy_pic).
                error(R.drawable.dummy_pic).
                into(holder.profile_image);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("request_id", String.valueOf(entity.getId()));
                mContext.startActivity(new Intent(mContext, HistoryDetailActivity.class).putExtra(AppConfig.EXTRA.requestId,entity.getId()));
            }
        });

        holder.image_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mContext.startActivity(new Intent(mContext, VoiceActivity.class).putExtra("checkOutgoing", 3));

//                mContext.startActivity(new Intent(mContext, VoiceActivity.class)
//                        .putExtra(AppConfig.BUNDLE.passenger_first_name, firstname)
//                        .putExtra(AppConfig.BUNDLE.passenger_last_name, lastname)
//                        .putExtra(AppConfig.BUNDLE.PassengerUniqueCode, uniqueCode)
//                        .putExtra(AppConfig.BUNDLE.display_passenger_name, displaypassengername)
//                        .putExtra(AppConfig.EXTRA.CHECKOUTGOING, 1));
            }
        });

        Log.d(TAG, "is schedule :" + entity.getIsSchedule());

        if (entity.getIsSchedule()==1) {
            holder.ll_instruction_details.setVisibility(View.VISIBLE);
        }
        else {
            holder.ll_instruction_details.setVisibility(View.GONE);
        }

        holder.image_navigate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double sourceLat = Double.parseDouble(entity.getSource_latitude());
                double sourceLang = Double.parseDouble(entity.getSource_longitude());

                String url = "google.navigation:q=" + sourceLat + "," + sourceLang + "&mode=d";
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                mContext.startActivity(intent);

            }
        });

        if (TextUtils.isEmpty(entity.getRideInstruction())
                || entity.getRideInstruction() == null) {

//            holder.tv_pre_schedule_instruction.setText(mContext.getResources().getString(R.string.no_instructions));
        }
        else {
            holder.tv_pre_schedule_instruction.setText(entity.getRideInstruction());
            holder.tv_pre_schedule_instruction.setInputType(InputType.TYPE_TEXT_FLAG_MULTI_LINE);

        }

        holder.image_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (TextUtils.isEmpty(entity.getRideInstruction())) {
//                    Toast.makeText(mContext, "" + mContext.getString(R.string.no_instructions), Toast.LENGTH_SHORT).show();
                } else {
//                    openMoreInstructions();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return histories.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_name, tv_status,
                tv_source_location, tv_destination_location,tv_display_name,
                tv_completed_time, tv_total_cost,tv_pre_schedule_instruction;
        LinearLayout ll_instruction_details;
        CircleImageView profile_image;

        ImageView image_navigate , image_call , image_more;

        ViewHolder(View view) {
            super(view);

            tv_name = view.findViewById(R.id.tv_name);
            tv_status = view.findViewById(R.id.tv_status);
            tv_source_location = view.findViewById(R.id.tv_source_location);
            tv_destination_location = view.findViewById(R.id.tv_destination_location);
            tv_completed_time = view.findViewById(R.id.tv_completed_time);
            tv_total_cost = view.findViewById(R.id.tv_total_cost);
            tv_display_name = view.findViewById(R.id.tv_display_name);
            tv_pre_schedule_instruction = view.findViewById(R.id.tv_pre_schedule_instruction);
            profile_image = view.findViewById(R.id.profile_image);

            image_navigate = view.findViewById(R.id.image_navigate);
            image_call = view.findViewById(R.id.image_call);
            image_more = view.findViewById(R.id.image_more);
            ll_instruction_details = view.findViewById(R.id.ll_instruction_details);
        }
    }
}
