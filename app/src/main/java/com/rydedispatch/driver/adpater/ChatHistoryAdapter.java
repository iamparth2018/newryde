package com.rydedispatch.driver.adpater;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.rydedispatch.driver.R;
import com.rydedispatch.driver.model.Message;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

public class ChatHistoryAdapter extends RecyclerView.Adapter<ChatHistoryAdapter.ViewHolder>{

    private ArrayList<Message> messageArrayList ;

    public ChatHistoryAdapter(ArrayList<Message> messageArrayList){
        this.messageArrayList = messageArrayList;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.row_chat, viewGroup, false);
        return new ViewHolder(view);
    }
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int i) {
        Message history = messageArrayList.get(i);
        if(history.getDirection() == 0){
            holder.ll_item.setGravity(Gravity.START);
            holder.tv_from_name.setText(R.string.support_rydecabbie);
        }else{
            holder.ll_item.setGravity(Gravity.END);
            holder.tv_from_name.setText(String.format("%s %s", history.getFirst_name(), history.getLast_name()));
        }

        holder.tv_message.setText(history.getMessage());

        String createdAt = history.getUpdated_at();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss", Locale.US);
        Calendar calendar = Calendar.getInstance();
        try {
            calendar.setTime(simpleDateFormat.parse(createdAt));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        int hoursCompleted = calendar.get(Calendar.HOUR_OF_DAY);
        int minsCompleted = calendar.get(Calendar.MINUTE);

        holder.tv_time.setText(String.format(Locale.getDefault(),"%d:%d", hoursCompleted, minsCompleted));

    }
    @Override
    public int getItemCount() {
        return messageArrayList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_from_name,tv_message,tv_time;
        LinearLayout ll_item;
        ViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_from_name = itemView.findViewById(R.id.tv_from_name);
            tv_message = itemView.findViewById(R.id.tv_message);
            tv_time = itemView.findViewById(R.id.tv_time);
            ll_item = itemView.findViewById(R.id.ll_item);
        }
    }
}
