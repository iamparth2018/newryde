package com.rydedispatch.driver.app;

import android.app.Application;
import android.os.Build;
import android.os.StrictMode;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;

import static com.android.volley.VolleyLog.TAG;

public class MyApp extends Application {
    private static MyApp singleton;

//    public String BASE_URL = "http://159.203.105.17/api/";
//    public String BASE_URL = "https://portal.rydedispatch.com/api/";
    public String BASE_URL = "http://167.172.247.117//api/";
//    public String BASE_IMAGE_URL = "http://159.203.105.17/";
//    public String BASE_IMAGE_URL = "https://portal.rydedispatch.com/";
    public String BASE_IMAGE_URL = "http://167.172.247.117/";

    private RequestQueue mRequestQueue;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        singleton = this;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                    .detectNonSdkApiUsage()
                    .penaltyLog()
                    .build());
        }
    }

    public static MyApp getInstance(){
        return singleton;
    }

    private RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

}
