package com.rydedispatch.driver.utility;

import android.content.Context;
import android.content.SharedPreferences;

public class PreferenceHelper {

    private SharedPreferences prefs;

    public PreferenceHelper(Context ctx) {
        prefs = ctx.getSharedPreferences(ctx.getPackageName(), Context.MODE_PRIVATE);
    }

    public boolean isAppLaunched(){
        return prefs.getBoolean("firstRun",false);
    }

    public void setAppLaunched(boolean b){
        prefs.edit().putBoolean("firstRun",b).apply();
    }

    public String getTwilioAuth(){
        return prefs.getString("TwilioAuth","");
    }

    public void setTwilioAuth(String user){
        prefs.edit().putString("TwilioAuth",user).apply();
    }

    public boolean isLogin(){
        return prefs.getBoolean("isLogin",false);
    }

    public void setLogin(boolean b){
        prefs.edit().putBoolean("isLogin",b).apply();
    }

    public String getUserUniqueCode(){
        return prefs.getString("user_unique_code","");
    }

    public void setUserUniqueCode(String id){
        prefs.edit().putString("user_unique_code",id).apply();
    }

    public String getFirstName(){
        return prefs.getString("FirstName","");
    }

    public void setFirstName(String user){
        prefs.edit().putString("FirstName",user).apply();
    }

    public String getLastName(){
        return prefs.getString("LastName","");
    }

    public void setLastName(String user){
        prefs.edit().putString("LastName",user).apply();
    }

    public String getEmail(){
        return prefs.getString("EmailId","");
    }

    public void setEmail(String email) {
        prefs.edit().putString("EmailId",email).apply();
    }

    public String getPhoneNumber(){
        return prefs.getString("PhoneNumber","");
    }

    public void setPhoneNumber(String s) {
        prefs.edit().putString("PhoneNumber",s).apply();
    }

    public String getUserPic(){
        return prefs.getString("ProfilePic","");
    }

    public void setUserPic(String pic){
        prefs.edit().putString("ProfilePic",pic).apply();
    }

    public String getAuth(){
        return prefs.getString("UserAuth","");
    }

    public void setAuth(String user){
        prefs.edit().putString("UserAuth",user).apply();
    }

    public String getDriverName(){
        return prefs.getString("driverName","");
    }

    public void setDriverName(String id){
        prefs.edit().putString("driverName",id).apply();
    }

    public String getDriverPic(){
        return prefs.getString("driverPic","");
    }

    public void setDriverPic(String id){
        prefs.edit().putString("driverPic",id).apply();
    }

    public String getClientName(){
        return prefs.getString("ClientName","");
    }

    public void setClientName(String id){
        prefs.edit().putString("ClientName",id).apply();
    }

    public String getDriverUniqueCode(){
        return prefs.getString("driver_unique_code","");
    }

    public void setDriverUniqueCode(String id){
        prefs.edit().putString("driver_unique_code",id).apply();
    }

    public String getUserStatus(){
        return prefs.getString("UserStatus","");
    }

    public void setUserStatus(String id){
        prefs.edit().putString("UserStatus",id).apply();
    }

    public void clearAllPrefs() {
        prefs.edit().clear().apply();
    }
}