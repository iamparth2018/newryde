package com.rydedispatch.driver.utility;

import android.app.ActivityManager;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import com.rydedispatch.driver.R;
import com.rydedispatch.driver.app.MyApp;
import java.util.List;

public class AppConfig {

    public static final String URL_SEND_LOGIN_OTP = "sendLoginOTP";
    public static final String URL_LOGIN = "login";
    public static final String URL_SEND_OTP = "sendOTP";
    public static final String URL_CREATE_DRIVER = "createDriver";
    public static final String URL_CHANGE_STATUS = "updateAvailability";
    public static final String URL_DRIVER_DASHBOARD = "driverDashboard";
    public static final String URL_HISTORY_LIST = "driverHistory";
    public static final String URL_HISTORY_DETAILS = "historyDetails";

    public static final String URL_PAST_HISTORY = "passengerHistory";
    public static final String URL_RIDE_DETAIL = "historyDetails";
    public static final String URL_DASHBOARD = "dashboard";

    public static final String URL_GET_CARD= "getCard";
    public static final String URL_ADD_CARD= "addCard";
    public static final String URL_DELETE_CARD= "deleteCard";

    public static final String URL_GET_PROFILE= "getProfileDetails";
    public static final String URL_UPDATE_PROFILE= "updateProfile";

    public static final String URL_SEND_MESSAGE = "sendMessage";
    public static final String URL_GET_MESSAGE = "getMessages";

    public static final String URL_GET_AUDIO_TOKEN = "generateAudioToken";
    public static final String URL_SUBMIT_RATING = "submitRating";
    public static final String URL_SEND_EMAIL = "emailInvoice";

    public static final String URL_GET_CARS = "findCarTypes";
    public static final String URL_RIDE_SUMMARY = "rideSummary";
    public static final String URL_CANCEL_RIDE = "cancelRideRequest";
    public static final String URL_RIDE_DETAILS = "rideDetails";
    public static final String URL_RIDE_REQUEST = "rideRequest";
    public static final String URL_CHECK_CODE = "promocode";

    public static final class EXTRA {
        public static final String requestId = "request_id";
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        assert connectivityManager != null;
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static String getImageUrl(String url){
        Uri path = Uri.parse("android.resource://com.rydemate.user/" + R.drawable.dummy_pic);
        String finalUrl = path.toString();
        if(url!=null){
            if (url.length()>0){
                if(!url.equalsIgnoreCase("null")){
                    if(url.contains("http")){
                        finalUrl = url;
                    }else{
                        finalUrl = MyApp.getInstance().BASE_IMAGE_URL + url;
                    }
                }
            }

        }
        return finalUrl;
    }


    public static boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
        for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
            if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                for (String activeProcess : processInfo.pkgList) {
                    if (activeProcess.equals(context.getPackageName())) {
                        isInBackground = false;
                    }
                }
            }
        }

        return isInBackground;
    }
}
