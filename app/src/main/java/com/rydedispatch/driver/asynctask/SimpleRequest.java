package com.rydedispatch.driver.asynctask;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;

import androidx.appcompat.app.AlertDialog;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.firebase.iid.FirebaseInstanceId;
import com.rydedispatch.driver.R;
import com.rydedispatch.driver.app.MyApp;
import com.rydedispatch.driver.utility.PreferenceHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class SimpleRequest {

    private String mUrl;
    private Callbacks mCallbacks;
    private Context context;
    private PreferenceHelper helper;
    private ProgressDialog progressDialog;

    public SimpleRequest(Context c, String url, Callbacks callbacks){
        this.mUrl = url;
        this.context = c;
        this.mCallbacks = callbacks;
        helper = new PreferenceHelper(context);
        progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(context.getResources().getString(R.string.pls_wait));
        progressDialog.show();

        Log.e("OnResponse ", helper.getAuth());
    }

    public void doInBackEnd(){
        StringRequest strReq = new StringRequest(Request.Method.POST, MyApp.getInstance().BASE_URL + mUrl, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                dismissDialog();
                Log.e("OnResponse "+mUrl, response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if(!jsonObject.getBoolean("error")){
                        mCallbacks.onSuccess(jsonObject);
                    }else{
                        if(jsonObject.getString("message").equalsIgnoreCase("Unauthenticated")){
                            try {
                                FirebaseInstanceId.getInstance().deleteInstanceId();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            logoutDialog(context);
                        }else{
                            mCallbacks.onFail(jsonObject.getString("message"));
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("onError ",mUrl+ error.getMessage());
                dismissDialog();
                mCallbacks.onFail(error.getMessage());
            }
        }){
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Accept","application/json");
                headers.put("Authorization", "Bearer "+helper.getAuth());
                return headers;
            }
        };

        MyApp.getInstance().addToRequestQueue(strReq, "string_req");

    }

    private void logoutDialog(final Context context){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(R.string.auth_title);
        builder.setMessage(R.string.auth_msg);
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                helper.clearAllPrefs();
//                context.startActivity(new Intent(context, LoginActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
            }
        });
        builder.show();
    }

    public interface Callbacks {
        void onFail(String error);
        void onSuccess(JSONObject response);
    }

    private void dismissDialog(){
        ((Activity) context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(!((Activity) context).isDestroyed() && !((Activity) context).isFinishing()){
                    if (progressDialog!=null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }
                }

            }
        });
    }
}
